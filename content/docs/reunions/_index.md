---
title: Les réunions hebdomadaires
bookCollapseSection: true
---

Toutes les semaines le dimanche a 21h (heure française) on se retrouve sur le vocal pour discuter de la vie du serveur et de son organisation. Ces réunions sont ouvertes à toutes et tous.

