---
title: Ethique Numérique
bookCollapseSection: true
weight: 5140
---

# Université du Canard Confiné 2020

## Comment retrouver mes valeurs éthiques dans mes outils numériques ?

    Samedi 5 Décembre, 14h

Alors que notre environnement se numérise de plus en plus et qu'il semble devenir indispensable d'utiliser des outils numériques au quotidien, une question se pose : Qu'en est-il de l'éthique de tout cela ?

Nos données, personnelles ou non, avivent les marchés du monde entier, que l'économie de l'attention semble se situer dans tous les recoins d'internet, comment pouvons-nous aborder le numérique en y associant nos principes moraux ?

Cette question sera abordée sous la forme d'un question-réponse, en compagnie des copines et copains de Framasoft.

### A propos de Framasoft

Framasoft est un réseau d'éducation populaire créé en novembre 2001 par Alexis Kauffmann, Paul Lunetta, et Georges Silva, et soutenu depuis 2004 par l'association homonyme. Consacré principalement au logiciel libre, il s'organise en trois axes sur un mode collaboratif : promotion, diffusion et développement de logiciels libres, enrichissement de la culture libre et offre de services libres en ligne. [ref wikipedia][2]

- Intervenant: [Framasoft][1]
- Live: *à venir*
- Enregistrement: *à venir*

[1]: https://framasoft.org
[2]: https://fr.wikipedia.org/wiki/Framasoft

---

<< [retour au programme](../programme)
