---
title: Emancipation Information
bookCollapseSection: true
weight: 6200
---

# Université du Canard Confiné 2020

## S’émanciper de l’information

    Dimanche 6 Décembre, 20h

[Préambule (brouillon) : S'émanciper de l'information (PDF - 30 pages)](/pdf/Universite_des_Canards_Refractaires_Semanciper_de_linformation_ADuclos.pdf)

- Intervenant: [Alexandre Duclos][1]
- Live: *à venir*
- Enregistrement: *à venir*

[1]: https://www.youtube.com/channel/UChMTDpfZ9HcXB0K_MHhd_ag

---

<< [retour au programme](../programme)
