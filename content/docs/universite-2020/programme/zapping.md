---
title: Zapping critique
bookCollapseSection: true
weight: 6190
---

# Université du Canard Confiné 2020

## Mini-documentaire sur le zapping / Éducation média, Espace critique dans les médias

    Dimanche 6 Décembre, 19h

- Intervenant: [Le Pixel Mort][1]

Monteur vidéo depuis 2013, passionné par le paysage politique et audiovisuel français, j'ai officié sur la chaine "Zap Télé" jusqu'en aout 2018.

Les neurones grillés par l'ingurgitation de milliers d'heures de télévision, devenu une machine à fabriquer du zapping, je décide de voler de mes propres ailes et de monter ma chaine, "Le Pixel Mort", le 1er septembre 2018.

Disposant désormais d'une totale liberté dans la création de mes vidéos, j'ai l'intention de vous livrer une analyse au vitriol de notre société.    

- Live: *à venir*
- Enregistrement: *à venir*

[1]: https://www.youtube.com/channel/UCeXsZz8C5sUZe1YgDxR6j9A

---

<< [retour au programme](../programme)
