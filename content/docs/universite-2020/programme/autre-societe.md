---
title: Autre Société
bookCollapseSection: true
weight: 5180
---

# Université du Canard Confiné 2020

## Penser la société autrement

    Samedi 5 Décembre, 18h

Partons du postulat que l'état prend forme dès lors que deux facteurs sont présents :

- ceux qui y sont favorables
- ceux qui s'y opposent.

Tentons de nous placer hors de cette vision bipolaire de la question.
Il vous est ici proposé de creuser l'émancipation d'un système/état en passant par différentes questions et en débattant ouvertement de chacune d'entre-elles (à voir selon le temps consacré à chaque question) :

- Peut-on exister par le discrédit ?
- Faut-il penser une alternative au système actuel, et si oui, faut-il la penser à l'échelle d'un état ?
- Dans l'hypothèse d'un "nouveau" modèle de société viable, peut-on résister au milieu de pays néo-libéralistes dont l'unique moteur est un productivisme aveugle ?
- Modèles alternatifs théoriques/pratiques connus
- Remise en question des assertions développées au cours des échanges.
L'objectif serait de définir un modèle de société qui soit adaptable à un maximum de contextes, ou à minima d'en dessiner certains traits, permettre de dégager des pistes pour aller plus loin sans nous perdre dans l'opposition brute et la réaction plutôt que l'action.

### Sources

- https://www.cairn.info/revue-du-mauss-2016-2-page-5.htm#
- https://fr.theanarchistlibrary.org/library/revolution-liquide
- https://www.revue-ballast.fr/le-municipalisme-libertaire-quest-ce-donc/
- https://fr.theanarchistlibrary.org/library/murray-bookchin-le-municipalisme-libertaire
- https://fr.theanarchistlibrary.org/library/pierre-kropotkine-la-federation-comme-moyen-d-union

---

- Intervenant: Bountar
- Live: *à venir*
- Enregistrement: *à venir*

---

<< [retour au programme](../programme)
