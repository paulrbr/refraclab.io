---
title: Elections US
bookCollapseSection: true
weight: 6180
---

# Université du Canard Confiné 2020

## L’élection Présidentielle États-Unienne 2020 vue par un socialiste Américain

    Dimanche 6 Décembre, 18h

- Intervenant: Un militant socialiste américain
- Live: *à venir*
- Enregistrement: *à venir*

---

<< [retour au programme](../programme)
