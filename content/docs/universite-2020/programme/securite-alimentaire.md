---
title: Securité Alimentaire
bookCollapseSection: true
weight: 6100
---

# Université du Canard Confiné 2020

## La sécurité sociale alimentaire, le hacking territorial & présentation du week-end SSA

    Dimanche 6 Décembre, 11h

- Intervenant: Invivo et Denved ar vro des [Réfractaires][1]
- Live: *à venir*
- Enregistrement: *à venir*

[1]: https://discord.lacanardrefractaire.org/

---

<< [retour au programme](../programme)
