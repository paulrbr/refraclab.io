---
title: Action Militante
bookCollapseSection: true
weight: 5160
---

# Université du Canard Confiné 2020

## De l’Action militante !

    Samedi 5 Décembre, 16h

- Intervenant: Nina et Antoine de [Extinction Rebellion][1]

Extinction Rebellion est un mouvement mondial de désobéissance civile en lutte contre l’effondrement écologique et le réchauffement climatique lancé en octobre 2018 au Royaume-Uni. "Nous sommes… vous ! Vous êtes invité.e.s à nous rejoindre et à participer, si vous êtes d’accord avec nos principes et nos valeurs."



- Live: *à venir*
- Enregistrement: *à venir*

[1]: https://rebellion.global/

---

<< [retour au programme](../programme)
