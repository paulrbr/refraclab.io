---
title: Transréalité
bookCollapseSection: true
weight: 6130
---

# Université du Canard Confiné 2020

## L’intelligence collective, la transréalité, et comment sortir de la matrice.

Expérience de hacking philosophique. L’intelligence artificielle. Vers l’action citoyenne.

    Dimanche 6 Décembre, 13h

- Intervenant: [Vincent Cespedes][1], philosophe

Vincent Cespedes est un philosophe et un essayiste français. Entre l'intime et le politique, sa pensée s'organise autour de trois axes : la création de sens, la quête de l'efficience interpersonnelle et la critique sociale. 

- Live: *à venir*
- Enregistrement: *à venir*

[1]: https://www.youtube.com/user/Zapatoustra

---

<< [retour au programme](../programme)
