---
title: Media Militants
bookCollapseSection: true
weight: 5110
---

# Université du Canard Confiné 2020

## Les médias militants

    Samedi 5 Décembre, 11h

### Le Canard Réfractaire

Le Canard réfractaire, c'est un média collaboratif, populaire et militant né dans les Côtes-d'Armor. Un média par et pour le peuple qui se propose d'être un outil pour lutter contre la corruption des médias traditionnels. En gros : on se met au service du mouvement social et on sort la "lutte" de ses réseaux militants habituels.

Notre outil de prédilection, c'est la vidéo et la photo. Depuis Janvier, il y a aussi une version papier du Canard, calibré pour le local.

Le média a été créé en mai 2019 sur Guingamp, dans l’impulsion du mouvement social qu'incarnent si bien les Gilets Jaunes. Nous avons deux stratégies d'actions :

- Proposer du contenu vidéo d'analyse et d'actualité politique pour le grand nombre.
- Être actif sur le terrain du local pour permettre de fédérer les luttes existantes entre elles et soutenir des actions concrètes sur le territoire. Nous couvrons principalement Guingamp, Lannion, Saint-Brieuc et Rostrenen.

Ah, et puis on va pas faire de chichi : nous sommes des défenseurs d'une révolution démocratique, écologique, sociale et populaire. Voilà, on pose ça dès le début, comme ça c'est fait!

- sur [youtoube][1]
- sur [le peertube canard.tube][2] c'est carrément mieux
- [tipee][9]
- [site web][7]
- [mailing-list debordons][10]

### Etat Cryptique

État cryptique est une chaîne à la fois militante et de vulgarisation juridique et politique. On y parle d'institutions, de la Constitution, du fonctionnement de la République et de ses dysfonctionnements, de mouvements sociaux, etc. L'objectif est de questionner tout ça, prendre du recul, faire un pas de côté. Aymeric y partage des solutions et des questions sur la démocratie.

- sur [youtube][3]

### Kalee Vision

Lisandru est animateur d'une radio libre sur YouTube sous le nom de Kalee Vision. Militant engagé il réalise des vidéos en live contenants des revues de presse, interviews, tables rondes, enquêtes ...

- sur [youtube][4]
- sur [anchor][5]
- la [communauté Kalee sur discord][6]

[1]: https://www.youtube.com/channel/UCWGsN59FON3vOtXCozQPsZQ
[2]: https://canard.tube
[3]: https://www.youtube.com/channel/UC86CmS3IpmQYxcjbv5eh7PA
[4]: https://www.youtube.com/channel/UCrHMeoPLDWw-_xfFMN-585g
[5]: https://anchor.fm/kaleevision
[6]: http://discord.me/kaleevision
[7]: https://lecanardrefractaire.org
[8]: https://www.instagram.com/lecanardrefractaire/
[9]: https://fr.tipeee.com/lecanardrefractaire
[10]: https://lecanardrefractaire.org/debordons/]

---

<< [retour au programme](../programme)
