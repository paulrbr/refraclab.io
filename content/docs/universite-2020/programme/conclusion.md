---
title: Conclusion
bookCollapseSection: true
weight: 6210
---

# Université du Canard Confiné 2020

## Conclusion: on refait le monde

    Dimanche 6 Décembre, 21h

- Intervenant: Yohan du [Canard Réfractaire][1]
- Live: *à venir*
- Enregistrement: *à venir*

[1]: https://lecanardrefractaire.org

---

<< [retour au programme](../programme)
