---
title: Droits et Luttes aux US
bookCollapseSection: true
weight: 6170
---

# Université du Canard Confiné 2020

## 	Les droits civiques aux USA - Moyen de luttes actuels + Discussion

    Dimanche 6 Décembre, 17h

- Intervenant: Olivier Azam des [Mutins de Pangée][1]

Les mutins de Pangée est une coopérative audiovisuelle et cinématographique de production, d’édition et de distribution (en salles, DVD, VOD).

Olivier Azam est un réalisateur français de films documentaires. Il est également directeur de la photographie, acteur, monteur, compositeur et producteur de cinéma

- Live: *à venir*
- Enregistrement: *à venir*

[1]: https://www.lesmutins.org/

---

<< [retour au programme](../programme)
