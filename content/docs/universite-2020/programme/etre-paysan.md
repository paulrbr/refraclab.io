---
title: Etre Paysan
bookCollapseSection: true
weight: 6120
---

# Université du Canard Confiné 2020

## La cultivation de la vie: être paysan et être heureux & volontariat solidaire!

    Dimanche 6 Décembre, 12h


L’engagement de Sanglier Jaune s’organise en 2 parties :

**Résister autrement**

Votre Sanglier change de vie et rejoint une équipe dans la France profonde pour aider au développement de projets locaux qui nous ressemblent dans une démarche de lutte globale cohérente.
Des sortes de bases arrières, des lieux où nous voulons pouvoir accueillir et faire venir du monde à travers la France pour se former, s’autonomiser, se préparer, s’organiser.

**Les projets Vidéos**

Des reportages et des œuvres avec une marque de fabrique, imaginées comme de vraies passerelles pour s’impliquer et s’éveiller.
Intervenant: [Le Sanglier Jaune][1]

- Live: *à venir*
- Enregistrement: *à venir*

[1]: https://www.youtube.com/channel/UCIJffkItaprUTFmDNOMXYxA

---

<< [retour au programme](../programme)
