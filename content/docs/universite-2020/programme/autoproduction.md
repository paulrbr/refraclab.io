---
title: Autoproduction
bookCollapseSection: true
weight: 5190
---

# Université du Canard Confiné 2020

## 	Autoproduction populaire

    Samedi 5 Décembre, 19h

- Intervenant: Yohan du [Canard Réfractaire][1]
- Live: *à venir*
- Enregistrement: *à venir*

[1]: https://lecanardrefractaire.org

---

<< [retour au programme](../programme)
