---
title: Programme
bookCollapseSection: true
description: Programme de l'Université du Canard Confiné 2020
---

# Programme de l'Université confinée 2020

(23 novembre) Ce programme est encore en cours de validation, certaines interventions peuvent être amenées a changer...

Heure  | Intervenants          | Sujet
-------|-----------------------|------------------------------------
&nbsp; |                       | <h2 style="margin:0">Samedi 5 Décembre</h2>
10h    | les&nbsp;Réfractaires | [Présentation de l'équipe orga discord et de comment nous avons organisé collectivement l'université populaire](ouverture)
11h    | Canard&nbsp;Réfractaire<br>Etat&nbsp;Cryptique<br>Kalee&nbsp;Vision | [Les médias militants](media-militants)
12h    | Vécu                  | *à confirmer*
13h    | Franck Lepage         | [L'éducation populaire: quoi, pourquoi, comment et les conférences gésticulées](educ-pop)
14h    | Framasoft             | [Comment retrouver mes valeurs éthiques dans mes outils numériques ?](ethique-numerique)
15h    | Diable Positif        | [Diable au positif](lediable)
16h    | Extinction Rebellion  | [De l'Action militante !](action-militante)
17h    | *Table ronde*         | [Les modèles alternatifs au capitalisme: l'éducation populaire, et après...?](table-ronde-1)
18h    | Bountar               | [Penser la société autrement](autre-societe)
19h    | Canard Réfractaire<br>Yohan | [Autoproduction populaire](autoproduction)
20h    | Thomas Babord         | [Religion, laicité, athéisme: on fait le point!](laicite)
21h    | Viktor Dedaj          | [La crucifiction de Julian Assange](assange)
21h    |                       | Soirée film, programmation par les mutins de Pangée
23h    |                       | after party sur discord, et dodo pour les vieux
&nbsp; |                       | <h2 style="margin:0">Dimanche 6 Décembre</h2>
10h    | les&nbsp;Réfractaires | Ouverture, café, mise en train
11h    | Invivo<br>Denved ar vro | [La sécurité sociale alimentaire, le hacking territorial & présentation du week-end SSA](securite-alimentaire)
12h    | Le Sanglier Jaune     | [La cultivation de la vie: être paysan et être heureux & volontariat solidaire!](etre-paysan)
13h    | Vincent Cespedes      | [L'intelligence collective, la transréalité, et comment sortir de la matrice. Expérience de hacking philosophique. L'intelligence artificielle. Vers l'action citoyenne.](transrealite)
14h    | Etienne Chouard       | [Débat : Qui est légitime pour fixer les règles de la représentation (la Constitution) ? Les représentés ou les représentants ? Le RIC : oui ou non ?](constitution)
15h    |   | *... continuation ...*
16h    | *Table ronde*         | *Débat réflexion avec les conférenciers: On fait quoi ensuite*<br>Conferenciers: DataGueule, Etienne Chouard?, Alexandre Duclos<br>[Democratie, information, militantisme, utopie](table-ronde-2)
17h    | les mutins de Pangée<br>Olivier Azam | [Les droits civiques aux USA - Moyen de luttes actuels + Discussion](droits-et-luttes)
18h    | Un militant socialiste américain      | [L'élection Présidentielle États-Unienne 2020 vue par un socialiste Américain](elections-us)
19h    | Pixel Mort<br>(+ Crapulax?) | [Mini-documentaire sur le zapping / Éducation média, Espace critique dans les médias](zapping)
20h    | Alexandre Duclos      | [S'émanciper de l'information](information)
21h    | Yohan                 | [Conclusion: on refait le monde](conclusion)
22h    |                       | Soirée film, programmation par les mutins de Pangée
23h    |                       | after party sur discord
