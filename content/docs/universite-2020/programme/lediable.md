---
title: Diable
bookCollapseSection: true
weight: 5150
---

# Université du Canard Confiné 2020

## Thème à définir

    Samedi 5 Décembre, 15h

Tommy Tahir créateur de la chaine le diable positif vous présente des dessins animés axés sur l'actu afin de comprendre comment les humains foutent tout en l'air !



- Intervenant: [Le Diable au positif][1]

[1]: https://www.youtube.com/channel/UCI_FnSJPNGd3Y3IL0PwD-NQ

---

<< [retour au programme](../programme)
