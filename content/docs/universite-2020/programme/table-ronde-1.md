---
title: Alternatives Capitalisme
bookCollapseSection: true
weight: 5170
---

# Université du Canard Confiné 2020

## *Table ronde*<br>Les modèles alternatifs au capitalisme: l’éducation populaire, et après … ?

    Samedi 5 Décembre, 17h

- Table ronde ouverte à tous les intervenants
- Live: *à venir*
- Enregistrement: *à venir*


---

<< [retour au programme](../programme)
