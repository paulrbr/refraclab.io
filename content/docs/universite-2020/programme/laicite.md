---
title: Laïcité
bookCollapseSection: true
weight: 5200
---

# Université du Canard Confiné 2020

## Religion, laicité, athéisme: on fait le point

    Samedi 5 Décembre, 20h

- Intervenant: [Thomas Babord][1]

Militant, youtuber et vulgarisateur politique. Ami du Canard.

- Live: *à venir*
- Enregistrement: *à venir*

[1]: http://thomas-babord.surge.sh/
---

<< [retour au programme](../programme)
