---
title: Ouverture
bookCollapseSection: true
weight: 5100
---

# Université du Canard Confiné 2020

## Ouverture et accueil

    Samedi 5 Décembre, 10h

### Accueil et mise en place

A 10h nous commencerons à ouvrir les canaux vocaux, et l'équipe des réfractaires, aidé des petits débrouillards, sera à votre disposition pour régler les aspects techniques. Peut-être que discord est compliqué ? N'hésitez pas à demander de l'aide.

### Présentation de l'organisation du discord et de l'université

Vers 10h40 - Présentation de l'équipe orga formée sur le [discord réfractaire][2] et de comment nous avons organisé collectivement l'université populaire, par Yohan.

### Les petits Débrouillards

Le réseau des [Petits Débrouillards][3] participe du renouveau permanent de l'éducation populaire. Par une éducation aux démarches scientifiques, expérimentales et raisonnées, il contribue depuis 1986 à développer l’esprit critique, et à élargir les capacités d’initiatives de chacune et chacun. Son objectif est de permettre aux jeunes et moins jeunes de s’épanouir individuellement et collectivement, par des parcours de citoyenneté active et démocratique.

- Intervenant: Yohan du [Canard Réfractaire][1]
- Live: *à venir*
- Enregistrement: *à venir*

[1]: https://lecanardrefractaire.org
[2]: https://discord.lecanardrefractaire.org
[3]: https://www.lespetitsdebrouillards.org/

---

<< [retour au programme](../programme)
