---
title: Constitution et RIC
bookCollapseSection: true
weight: 6140
---

# Université du Canard Confiné 2020

## 	Débat : Qui est légitime pour fixer les règles de la représentation (la Constitution) ? Les représentés ou les représentants ? Le RIC : oui ou non ?

    Dimanche 6 Décembre, 14h

- Intervenant: [Etienne Chouard][1]

Étienne Chouard est un enseignant, blogueur, essayiste et militant politique français. Défenseur du Référendum d'initiative citoyenne, il a contribué aux discussions autour du sujet. Il propose des ateliers de réécriture de la constitution et a incarné une figure connue du mouvement des gillets jaunes.   

- Live: *à venir*
- Enregistrement: *à venir*

[1]: https://chouard.org

---

<< [retour au programme](../programme)
