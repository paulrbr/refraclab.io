---
title: Démocratie et Utopie
bookCollapseSection: true
weight: 6160
---

# Université du Canard Confiné 2020

## 	Table ronde: Débat réflexion avec les conférenciers: On fait quoi ensuite

Conferenciers: DataGueule, Etienne Chouard?, Alexandre Duclos

Democratie, information, militantisme, utopie

    Dimanche 6 Décembre, 16h

- Intervenant:
  - [DataGueule][1]
  - [Alexandre Duclos][2]
- Live: *à venir*
- Enregistrement: *à venir*

[1]: https://lecanardrefractaire.org
[2]: https://www.youtube.com/channel/UChMTDpfZ9HcXB0K_MHhd_ag

---

<< [retour au programme](../programme)
