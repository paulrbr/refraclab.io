---
title: Education Populaire
bookCollapseSection: true
weight: 5130
---

# Université du Canard Confiné 2020

## L’éducation populaire: quoi, pourquoi, comment et les conférences gésticulées

    Samedi 5 Décembre, 13h

- Intervenant: Franck Lepage
- Live: *à venir*
- Enregistrement: *à venir*


---

<< [retour au programme](../programme)
