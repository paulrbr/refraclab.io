---
title: Université confinée 2020
bookCollapseSection: true
description: Le Canard Réfractaire organise sa première édition de l’Université confinée sur Discord, avec Framasoft, Franck Lepage, Etienne Chouard, et plein d'autres intervenants. Conférences, tables rondes, débats.
images:
  - /img/univ-confine-2020.png
---

[![Affiche université confinée](/img/univ-confine-2020.png)](https://discord.gg/HaEpyY3)
# Les 5 et 6 Décembre 2020
## Université confinée du Canard Refractaire

**Le Canard Réfractaire** organise sa première édition de l’Université confinée sur Discord (événement pour célébrer la communauté réfractaire grandissante et les 50 000 abonnés Youtube).

**Au programme de l’Université:** conférences, ateliers, débats, discussions citoyennes ! Bonne ambiance garantie !

Vous retrouverez des thématiques telles que la parole citoyenne, les médias militants, l’environnement, l’autonomie alimentaire, la démocratie, la désobéissance civile ou l’émancipation de la société matérialiste pour retrouver une vie centrée sur le vivant.

Nous recevrons des invités tous plus intéressants les uns que les autres avec des noms comme :

- Framasoft
- Frank Lepage
- Le Pixel Mort
- Le Canard Réfractaire
- Vincent Cespedes
- Olivier Azam
- Extinction rébellion
- Thomas Babord
- Diable Positif 
- Les Mutins de Pangée
- Le Sanglier Jaune
- Etienne Chouard
- ... et pleins d'autres encore!

**L’université prendra place sur le Discord du Canard Réfractaire** les samedi 5 et dimanche 6 décembre 2020 sur le [discord des refractaires](https://discord.gg/HaEpyY3).

## Programme

- [Tous les détails sur le programme](programme)
## FAQ

### Est-ce que c'est obligé d'aller sur Discord ?

Il y a plusieurs moyens:

- Si vous voulez participer activement venez donc sur [discord][1]
- pour juste mater le live, il y a 3 endroits:
  - sur twitch sur https://www.twitch.tv/canardrefractaire, il sera possible de poser des commentaires et des questions
  - sur peertube sur https://live.canard.tube en lecture seule, pas d'interaction possible (fonctionnalité expérimentale)
  - sur youtoube sur une adresse qu'on mettra ici plus tard (commentaires désactivés)

Certaines sessions vont déborder un peu du programme et du coup le live ne couvrira que les sessions qui sont sur le programme. Mais on s'attend a ce que ca déborde un peu dans des canaux différents sur discord. Donc si vraiment vous voulez ne rien loupez, il va falloir aller sur discord.

### Est-ce que les sessions sont enregistrées ?

Oui, toutes les sessions sont enregistrées afin de pouvoir les partager par la suite, et publiés sur le [peertube du canard][2].

### Qui c'est qui organise tout ça ?

Cet événement est organisé de manière collective par les membres actifs du Discord du Canard Réfractaire. Ils se sont donnés pour but de créer durant 2 jours des espaces d'échanges pour promouvoir l'éducation populaire.

Au départ c'est Yohan qui [cherchait des idées][4] pour fêter les 50k abonnés a sa chaîne youtube, fin octobre. Du coup on en discuté lors de nos discussions hebdomadaires le [dimanche 1 novembre][3], et depuis, de manière


### Elle est ou la machine a café ?


[1]: https://discord.gg/HaEpyY3
[2]: https://canard.tube
[3]: https://discord.lecanardrefractaire.org/docs/reunions/2020-11-01/
[4]: https://discord.com/channels/633753846791012352/699864966047596604/771776594787827722
