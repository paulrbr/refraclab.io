---
title: HS1-4 Diviser pour mieux régner
weight: 4
---
# CR-HS1_4-diviser-pour-régner

Réflexion générale sur Diviser pour mieux régner hors contexte de la crise du Coronavirus :

## Division de l'extérieur d'un groupe

### Anti-Fa et compagnie

* Exemple de la division au sein des GJ (nuit debout à peu près pareil)
* Il a été observé des démarches d'éloignement des populos de droite pour les sortir des revendications et du Mvt. 
* De multiples tentatives de division avec l’appui des médias, les GJ sont des homophobes, antisémites, racistes, extrême droite, extrême gauche. (division externe)
* Le choix de la  successions des sujets d'analyse du mouvement a eu pour effet de focaliser les actes militants (destructifs) ou de cliver les GJ et de dresser au sein même des cercles familiaux et amicaux des divisions par assimilation à un groupe défini par ceux que ce mouvement gênait. La stigmatisation génère le clivages.

### Polémiques externes

* Les pièges tendu par les éléments de langage commun du gvmt et des 1%, exemple des retraites en parlant de "régimes d'exceptions" de "privilèges" ou la création de faux choix restreints "nous n'avons pas le choix" ou " il n'y a que deux solutions et pas sûr que vous désiriez la première solution".

### Divers

* Notion de pragmatisme économique : il faut réduire les lits, il faut réduire les régimes spéciaux, au final en la faveur des classes dominantes (ou possédantes au détriment de l'intérêt public (des plus pauvre, du peuple) 
* Abus de langage (glissement sémantique) : Défendre l’intérêt commun = défendre le pragmatisme économique
* Polémique pour discréditer l'ennemi -> matraquage dans les médiats 
* pompier pyromane : appauvrir les services public pour mieux les achever
* Stratégie des manipulations de masse Chomsky  ???  
* dont Stratégie du différé : faire passer une loi pour la mettre en vigueur plus tard quand ça arrange
* a voir: la fabrique du consentement: <https://www.dailymotion.com/video/x6kqf6i>
* fabrique du consentement animation: <https://www.youtube.com/watch?v=34LGPIXvU5M>
* inside the box : chaîne YT
* machiavel :
  * on n'a completement oublier de parler (enfin je crois) du fait qu'une minorité de tres riches font croire a une majorité de personne moyennes et modestes que le probleme financier vient des plus defavorisés des plus pauvres

## Division interne au groupe

### Les Polémiques internes du groupe

* Au seins même de nos groupes (amitié, famille, association, collègues) notre propre nature (a vouloir se taper sur la gueule une fois à l’abri du groupe) nous pousse naturellement à nous diviser entre nous, ils est tellement difficile de conserver une harmonie dans le groupe, le mimétisme, la volonté d'acquérir du pouvoir de certains au sein du groupe. vecteur de division dans le groupe 
* (individualistes, contestataires, indociles, indomptables, récalcitrants, rétifs, hégémonie, antagonistes... )
* Désaccord sur les modes d'action (nuit debout, GJ, *syndicats*)
* Complotistes, qui foutent la merde dans le groupe (les stratèges)
* Séparation des pouvoirs, 
* intérêts personnels dans le groupe 

### Un tendance très naturelles chez l'humain :

* Le conflit mimétique et la logiques sacrificielles (cf René Girard). L'origine du conflit de la division, se base en premier lieu sur une ressource qui est limité et qui viens à manquer pour certain (l'argent, le pétrole, le temps, la visibilité, l'accès à une ressource dite vitale ou même le confort). Ce mécanisme mimétique mené bien souvent à la désignation d'une minorité (bouc-émissaire) qui sera elle, sacrifiée pour résoudre la problématique soulevée par le groupe dominant.
* C'est ici un déni d’altérité.
* Dialectique du maître et de l’esclave de Hegel (phénoménologie de l'esprit)
* Les grandes écoles (l’élitisme séparer les gens distanciations sociales perpétuation d'une conscience de classe : vous êtes les meilleurs...)

## Division systémique

### La nature même du Capitalisme

* compétition = opposition dans la survie  
* Stimulation de la rareté, création de richesse sans valeur ajoutée
* L'amour pour l'élite : ne tapons pas sur notre modèle : cultiver la servitude de ceux qui admirent notre "dominant" (HS voir décadence culturelle)
* La division par le système de classe puis la mise en place du mépris de classe
* On souligne les division au sein même du peuple (classe moyen contre la classe inférieur) pour éviter de se focaliser sur la classe supérieur

### L'attribution des rôles

* Expérimentation sociale : 
* Pour révéler ce qui mène à l'impossibilité de penser le collectif, évacuer l’empathie entres les êtres humains
* Milgram : <https://www.scienceshumaines.com/stanley-milgram-1933-1984-la-soumission-a-l-autorite_fr_22642.html>
* Stanford :
* Hanna Arendt : la banalité du mal  (les dirigeants fragmente par petits bouts le problème à traiter)
* Eichmann : <https://fr.wikipedia.org/wiki/Eichmann_%C3%A0_J%C3%A9rusalem>


### Rejet systémique des minorité pour créer de nouvelle divisions 

* Exemple de mose : Lepen vs mitérand : instrumentalisation du racisme pour diviser/fragmenter la base du peuple
* noyautage et scission des syndicats : l'histoire du syndicalisme en France 
* voir Franck Lepage "Conf.Gest. sur le travail" (CFDT)

# Et la crise du Covid19 dans tout ça... 

## Le Covid19 est il une nouvelle source de division (mauvaise piste de réflexion)  

### Ceux qui dé-confine

* La division entre ceux qui se dé-confinent et ceux qui suivent la doctrine pour donner sa chance à une décision commune. Les tentions entre les personnes confinées qui "jouent le jeu" de la solution choisie par notre président et ceux qui estiment que le confinement est une privation de libertés et qui proteste contre cette doctrine imposée. <https://www.youtube.com/watch?v=JY6pBA3xyL0\&t=0s>
* Au final une fois dans nos conflit personnel on ne fais plus attention à qui choisi notre politique de gestion de crise collectivement, et pourquoi.
* (exemple : chiffres des dénonciations fermeture des standard de dénonciation plateforme nationale sud ouest)
* (incompréhension du choix de la politique claire ou éclairée par une parole ouverte et argumentée, cette politique à été imposée sans expliquer pourquoi)
* Consentement : expliciter la mécanique du consentement

### Qui doit survivre au Coronavirus ?

* Laissons mourir les vieux, les fragiles, les personnes sensible.
* Commençons à choisir qui pourra s'en sortir, ceux qui mettrons la main au portefeuille, ceux qui téléchargerons l'application
* Le coronavirus a-t-il acté la fin du multilatéralisme et le démantèlement de la communauté internationale? (c.f. détournement de matériels sanitaires) => division entre nations et impuissance de l'ONU
* Le coronavirus à également acté la division au sein même de l'union européenne

### Coronavirus et division

* Est-ce que le coronavirus est un vecteur de division ? non pas vraiment - tous pour un et dieux pour tous : démonstration d'un union de tous
* Est-ce que le coronavirus est instrumentalisé pour générer des division  ? non, pas sûr
* Est-ce que le dé-confinement pourrais générer de la division ? on ne peu pas statuer sur ce point c'est de la prospection
* En quoi le dé-confinement pourrais renforcer les divisions ? la question n'a pas vraiment d'intérêt pour les même raisons
* En quoi le dé-confinement pourrais créer de nouvelles divisions ? la question n'a pas vraiment d'intérêt pour les même raisons. Le dé-confinement risque de générer la frustration de certains, faire renaître de vieille divisions
* underlineConclusionunderline >>> la crise du Covid19 n'est qu'un "révélateur" des divisions préexistantes 
* Le Discours des média est-il diviseur vis a vis du corona virus? cette question semble déjà plus pertinente on va développer en dessous

## Discours des média est-il diviseur vis a vis du corona virus ?

* les média n'ont fait que révéler les divisions déjà présentes au sein de notre système de santé et parmi les chercheur
* la crise du Covid19 est un "révélateur" des divisions préexistantes
* exemple: l’Europe, pas de solidarité européenne
* naissance de la peur de l'autre de par la nature de la maladie : 
  * omment on va faire par la suite une fois dé-confinés
  * créer des nouvelle catégorie fragile, pas fragile, contaminant, pas contaminant.
* contre argument : avant le confinement les gens n'ont pas diminués leurs échanges

* les média sont par essence par définition par construction diviseur
* la division nourris les médiats qui ont besoin de faire de l’audience. Il faut etre le premier à donner l'info créant une pression sur le temps d'investigation
  * la différence de temporalité des médias et des autres secteurs (recherches scientifiques, politique publique) peut-il être un facteur de division? 

## Les points sur lesquels les média on essayer d'accès la divisions :

### mise en avant

* Créer des nouvelles catégorie invisible pour générer de la paranoïa : fragile vs pas fragile, contaminant vs pas contaminant.
* Naissance de la peur de l'autre de par la nature de la maladie.
* Ceux qui peuvent faire du télé-travail et ceux qui ne peuvent pas
* exemple de focus sur ceux qui ne respecte pas : bouhou! les méchants dans les citées, les parigo dans leur résidence secondaires...
* On souligne les divisions au sein même du peuple (classe moyen contre la classe inférieur) pour éviter de se focaliser sur la classe supérieur
* "Les chinois sont responsable de l'origine de la pendémie" (xénophobie asiatique avant la crise)

### mise en retrait (effacer, noyer, diluer)

* les testes, les masque, les gouvernement,
* les mesures sécuritaires pour notre bien, 
* le préfet l'allemand

---

# « Diviser pour mieux régner » grâce à une représentation polémique des débats sur les médias [par Null]***

Les définitions se confondent entre débat et démagogie.  
Une définition du débat bien présente dans les esprits :  
« Un débat est un échange opposant plusieurs candidats souhaitant attirer la faveur d'un groupe (et surtout avoir raison). »  
Sauf que cette définition est celle de la démagogie :  
« Attitude d’une personne qui cherche à s’attirer, par une complaisance excessive, la faveur d’un groupe. ».


Dans beaucoup d’esprit, démagogie et débat ne font qu'un, dans une télévision qui présente en pâture des hommes politiques, dont le seul et unique but est d'obtenir le crédit et l'approbation du public. La plupart des débats télévisés en politique sont des joutes verbales comparables à des clash de rappeurs, jouant davantage sur l'émotion et la forme des mots, que sur le sens et la cohérence des propos. Ici le but des politiques est de conquérir les cœurs, se faire élire. Est-ce véritablement du débat ?

Pour beaucoup, un débat, c’est se battre, ça doit péter et exploser dans tous les sens, il doit y avoir autant d’actions que dans un film américain, c’est passionné et polémique.  
Mais peut-on vraiment construire un monde meilleur pour tous, à coup de polémiques et de castagnes ?  
Et si on essayait de faire mieux que ça ?

Je crois que tout le monde possède une partie de la vérité et des solutions qui nous permettront de faire bouger les lignes de notre avenir, de part nos différents savoirs, nos différentes façons de pensées, de nos différents moyens de les mettre en œuvre les idées. De ce fait, c’est seulement en se mettant d’accord, en formulant ce qui nous réunit, qu’une population peut s’unir pour résoudre les problèmes qui mettent en danger notre avenir.

Seulement… le système tout entier semble inciter la division, à travers des élections, des votes, des polémiques, la recherche de débat-querelle, la mise en concurrence les uns avec les autres. On nous montre en permanence du conflit, de la guerre entre les idées, des combats glorifiés entre différents coqs aux couleurs d’un parti politique ou d’un autre.

On sait polémiquer, mais nous n'apprenons pas à décider collectivement, à nous unir, pour défendre les rêves que nous avons en commun, plutôt que d’accorder toute notre attention sur ce qui nous divise.


# Fin de la première partie de l'atelier, et suite de l'atelier en roue libre, faites vous plaisir...

les avis divergeant du moment sur le virus/ gestion de crise créent de la division  
absence de partage du savoir en ce moment  
opposition entre les pays ayant connu des crises de virus et les autres  

sujets de division:
* carte de dé-confinement
* télétravail
