---
title: HS1-8 nouvelles contraintes
weight: 8
---
# HS1-8 nouvelles contraintes

confinement, autorisations de sorties, pertes de congés, obligations de sur-travailler 60h rattrapage de productivité,
obligations vaccinales (perte de liberté de la disposition de soi-même) limitation des pratiques des sports individuels ou collectifs, manipulation des enfants, Instrumentalisation de l’éducation publique.

L'objectif de ce groupe de travail est de se pencher sur les attaques portées aux libertés individuelles, et aux contraintes imposées qui s'en suivent.

La quadrature du net ....

interim suspendue contrains au chômage ou a risquer leur vie.

Syndicat de la magistrature

position du Syndicat de la magistrature sur l'organisation des juridictions durant le confinement : http://www.syndicat-magistrature.org/Notre-vade-mecum-sur-vos-droits-pendant-le-confinement.html

- le droit de retrait
- l’autorisation d’absence exceptionnelle
- les congés

https://www.liberation.fr/debats/2020/04/28/la-justice-doit-redevenir-notre-bien-commun_1786605

http://www.syndicat-magistrature.org/IMG/pdf/note_etat_d_urgence_sanitaire.pdf = analyse juridique de l'état d'urgence
