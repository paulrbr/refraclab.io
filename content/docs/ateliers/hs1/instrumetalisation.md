---
title: HS1-2 Instrumentalisation de la Justice
weight: 2
---

# CRIMINALISATION :

- On a clairement en tête les militants de BURE qui ont été massivement surveillés
https://reporterre.net/1-3-La-justice-a-massivement-surveille-les-militants-antinucleaires-de-Bure
La gendarmerie interprétait les discussions recueillies pour requalifier des délits en crime.
- Un motard fauché par la portière d'une voiture de police. Il se fracture le tibia. 
https://www.liberation.fr/checknews/2020/04/19/que-sait-on-de-l-accident-de-moto-impliquant-la-police-a-villeneuve-la-garenne_1785701
L'homme était initialement connu par la police. La version initiale laissait entendre qu'il s'était pris la portière en fuyant sans préciser si la portière était ouverte ou si elle a été ouverte délibérément.

# ARRESTATION :

- Arrestation des récidivistes du non-respect du confinement
  - https://www.huffingtonpost.fr/entry/prison-ferme-confinement-coronavirus_fr_5e846b78c5b6871702a76de3
  - https://www.rtl.fr/actu/justice-faits-divers/confinement-six-mois-de-prison-ferme-pour-une-audoise-apres-des-infractions-7800365142
  - http://www.leparisien.fr/seine-et-marne-77/melun-deux-premiers-cas-de-prison-ferme-pour-non-respect-du-confinement-09-04-2020-8297208.php alors qu’en parallèle, on a libéré sur la même période presque 10 000 détenus
  - https://www.nicematin.com/sante/coronavirus-pres-de-10000-detenus-ont-ete-liberes-en-france-depuis-le-debut-de-lepidemie-496339 Une circulaire de mi-mars demandait de différer l’exécution des courtes peines d’emprisonnement.

# DÉTENTION

(attention sujet connexe mais pas directement lié au non respect du confinement, ce sont plus généralement les conséquences de l'organisation judiciaire par temps de Covid-19)

Contre-circulaire relative à l’adaptation de l’activité pénale des juridictions aux mesures de prévention et de lutte contre la pandémie de COVID-19  
http://www.syndicat-magistrature.org/Contre-circulaire-relative-a-l-adaptation-de-l-activite-penale-des-juridictions.html

Le syndicat de la magistrature donne ses préconisations sur la prolongation des durées de détentions provisoires (en cours d'instruction ou avant audience). A distinguer des peines déjà prononcées, qui concernent des détenus dont la sortie est souvent prononcée par anticipation.

Délai d'allongement de la détention provisoire pour crime et délit : 
- 2 mois si peine encourue inférieure ou égale 5 ans (en plus des 4 mois habituels) ; 
- 3 mois si peine encourue 7 ou 10 ans (en plus d'habituellement 1 an, voire 2 ans selon les cas);
- 6 mois si peine encourue > 10 ans  (en plus des habituels : 2, 3 voire 4 ans)

> Selon le Syndicat il faudrait considérer que le délai est allongé pour les seuls nouveaux cas postérieurs au 26 mars 2020, et non pour les cas en cours dans lesquels l'allongement du délai devrait respecter les règles habituelles d'allongement, ce qui permettrait de ne pas généraliser la détention arbitraire sous couvert de situation d'urgence sanitaire.

Le syndicat rappelle que le principe du respect des droits de la défense découle de la garantie des droits proclamée par l’article 16 de la DDHC  et implique notamment le droit à l’assistance effective de l’avocat au cours de la procédure pénale , le respect du principe du contradictoire et l’existence d’une procédure juste et équitable, garantissant l’équilibre des droits des parties. (décisions du Conseil Constitutionnel en ce sens)

> toutes ces garanties sont mises en danger par un allongement arbitraire de plein droit prévu depuis le 26/03/2020

Enfin, la Convention européenne des droits de l'homme pourra être invoquée : comment garantir l'effectivité des droits de la défense (article 6 de la CEDH) quand le délai (2 mois / 3 mois / 6 mois) ne permet aucun recours, sans compter la difficulté d'avoir un avocat qui lui-aussi subit le confinement, le difficile retrait d'une LRAR quand les bureaux de poste ferment...

# INTERDICTION

Dé-confinement

- Le Gouvernement souhaitait fermer les régions, ce qui aurait été incohérent avec le fait de laisser les frontières nationales ouvertes. Du coup, ils sont revenus dessus et ont limité les déplacements à 100km. 
- Interdit de se rassembler à plus de 10 personnes : qui arrange bien pour contrôler les Gilets Jaunes et autres.

Les interdictions incompatibles avec la crise sanitaire :

- Interdiction de vendre des masques en Pharmacie : alors que plusieurs personnes (autre que professionnels de santé) devaient continuer de travailler. Même les professionnels de Santé étaient rationnés au minimum (pour dire qu’ils en avaient fourni alors que le quota affecté à chaque professionnel était dérisoire). Le Gouvernement a finalement décidé de laisser les buralistes en vendre le 23 avril, se rendant compte de l’aberration (grâce à l’opinion publique) un décret sort le 26.04.2020 pour permettre aussi aux pharmacies d’en vendre. Finalement même les Grandes surfaces pourront en vendre à compter du 4 Mai 2020

https://www.courrier-picard.fr/id82489/article/2020-04-23/des-masques-reutilisables-5eu-vendus-chez-les-buralistes-partir-du-30-avril

Arrêté pour les Pharmacies : https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000041821967&categorieLien=id

Il s'agit de presque 500 millions de masques en vente libre or un tel stock venant de Chine ne se fait pas en 3 jours. Pourquoi n'a-t-il pas été réquisitionné initialement pour la survie des personnels soignants, pour ceux en premières lignes (éboueurs, caissiers,etc...) ou encore pour les personnes en EHPAD?

# AMENDE

Ce site recense les différentes verbalisations pour une bonne part abusives:
https://n.survol.fr/n/verbalise-parce-que

Une équipe de juristes et d'avocats ont créé un site pour contester ces amendes
https://pvconfinement.fr/

# FICHAGE

A ce jour, on veut faire passer l'utilisation de l'application du STOP-COVID afin de faire réguler la Pandémie et aller vers un dé-confinement total.
Or ouvrir un pont entre toutes nos données mobiles et L’État n'a pas toujours été une bonne chose pour la liberté de chacun.

Il a été rapporté qu'en matière de cybersécurité, on ne peut pas garantir à 100% la protection de données, selon l'article suivant
https://www.francebleu.fr/infos/societe/application-stopcovid-loin-de-proteger-nos-donnees-selon-eric-filiol-expert-lavallois-en-1587926172
Une fois le Bluetooth enclenché on peut récupérer l'adresse MAC et de là avoir accès à toutes informations contenues dans le mobile.

On voit bien avec d'autres applications telles que GENDNOTES ( application utilisées par la gendarmerie, la police et les renseignements)
https://www.laquadrature.net/2020/02/25/gendnotes-faciliter-le-fichage-policier-et-la-reconnaissance-faciale/
qu'il y a une dérive autoritaire et que même si initialement la cause semble juste...la mise en pratique est vite détournée de sa fonction initiale.

La loi  se peaufine et autorise l'intégration de photos à l'application, à suivre quand on sait que L’État a acheté des drones de surveillance:
https://nantes-revoltee.com/pendant-la-pandemie-le-gouvernement-commande-des-drones-et-des-gazeuses/

un article en anglais sur le déploiement de drones et autres moyens de surveillance justifiés par la pandémie : https://www.abc.net.au/news/2020-05-01/new-surveillance-technology-could-beat-coronavirus-but-at-a-cost/12201552 

## HEALTH DATA HUB

Suite à cette vidéo: https://youtu.be/Srbhq5p0_G8

On s'est renseigné sur les fondement des propos

En effet Health Data Hub collecte nos données sans notre consentement éclairé et avisé
https://buzz-esante.fr/health-data-hub-le-plan-strategique-devoile/

Le problème est que l'hébergement a été affecté sans appel d'offre par Stéphanie COMBES à Microsoft AZURE...une entreprise américaine ce qui signifie qu'en rapatriant les données françaises aux Etats-Unis, on ne subit plus les lois françaises (RGPD, CNIL) mais celles des Etats-Unis... or le 23.03.2018 a été adopté le Cloud Act aux Etats-Unis qui permet aux autorités Américaines de récupérer toutes données hébergées aux Etats-Unis même si elles sont étrangères.
- https://cnll.fr/news/h%C3%A9bergement-de-donn%C3%A9es-de-sant%C3%A9-du-health-data-hub-chez-microsoft/
- https://www.nextinpact.com/news/108781-health-data-hub-collectif-critique-choix-microsoft.htm
- https://www.conseil-national.medecin.fr/lordre-medecins/conseil-national-lordre/sante/donnees-personnelles-sante/health-data-hub
- https://www.april.org/tribune-pour-des-donnees-de-sante-au-service-des-patients
- https://information.tv5monde.com/info/donnees-numeriques-de-sante-le-health-data-hub-francais-cree-la-polemique-335083

Je rappelle qu'OVH,hébergeur Français répondant à toutes les normes RGPD, est agréé pour héberger toutes les données sensibles et confidentielles comme celles de la Santé et n'a même pas été consulté.
https://www.ovh.com/fr/files/2018-06/plaquette-gdpr-web-Final-French.pdf
"Nous disposons aussi d’un agrément pour l’hébergement de données de santé (HDS) pour notre offre Healthcare" (page 7)
