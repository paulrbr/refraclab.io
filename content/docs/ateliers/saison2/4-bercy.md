---
title: 4 - Bercy
---

( Comment le contrôle financier c'est organisé pour rester dans les mains d'une élite qui ne sert que ses propres intérêts en toute impunité. )

D'une très simple question (pourquoi certaines entreprises échappent à la fiscalité ; pourquoi organisation Bercy compatible avec cette situation ; pourquoi quelques 300 personnes depuis 40 ans à l'IGF ont la main mise sur ce berceau de Bercy) on a des réponses tentaculaires : problèmes en forme de pieuvre. 

## Problèmes :

- Secret des rémunérations / absence de contrôle
- Idéologie non explicite, commis de l'Etat (issus de l'appareil d'état et des plus grandes écoles publiques) qui pataugent dans le conflit d'intérêt : profitent à titre personnel de la privatisation des biens de l'Etat
- Absence de contre pouvoir : journalisme, associations, contribuables, entreprises = impuissance / pressions
- Dépassement clivage gauche/droite : Fabius, Juppé, Jospin, Sarkozy, Moscovici, Sapin etc
- Comment passe-t-on d'une entreprise publique à une entreprise privée soutenu par Bercy? (recenser les copinages etc) 
- Quand on dénonce = plainte ! <https://www.capital.fr/economie-politique/bercy-porte-plainte-apres-des-revelations-du-canard-enchaine-1135849>
- Droit pénal, protection des données fiscale qui empêche le contrôle.
- Avec nos impôts on donne des milliards (CICE, CIR, etc...) Et nous n'avons aucun moyen de contrôle sur l'utilisation des ces Impôts.
- Négociations possibles pour les fraudeurs fiscaux même après la loi fin 2018 qui a un peu atténué le verrou
<https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000039683923\&dateTexte=\&categorieLien=id>
<https://www.legifrance.gouv.fr/affichLoiPubliee.do?idDocument=JORFDOLE000039145703\&type=echeancier\&typeLoi=\&legislature=15>

## Ce qu'on sait déjà : 
- Il n'y a que des gens qui sortent des grandes écoles ... (ENA = inspection générale des finance: Fabuis, Juppé etc..)
- Certains se spécialisent dans les aller retour public-privé : 
  * Nicolas Namias (Natixis, BPCE),
  * Thierry Aulagnon (GAN Société Générale),
  * Alexis Kohler (MSC compagnie de frêt 2eme mondial),
  * Emmanuel Moulin (banque italienne en lien avec la privatisation de l'aéroport de Nice),
  * François Villeroy de Galhau* (Cetelem, BNP puis gouverneur de la banque de france / ACPR), 
  - Eric Lombard (Fusion BNP Parisbas puis Directeur Général de la Caissse des Dépôts similitude avec 
  - François Villeroy de Galhau* <https://www.caissedesdepots.fr/modele-unique/gouvernance/Eric-Lombard>  )
  - François Pérol (Rotschild), 
  - Marie-­Anne Barbat-Layani (Crédit Agricole) , 
  - Pierre Mariani (BNPP, Dexia), 
  - Jean Pierre Jouyet (Barclays \& AMF), 
  - Hubert Reynié (BNP, AMF, Crédit agricole)
  - Augustin de Romanet (Pdg d'ADP) <http://www.journaldunet.com/business/salaire/patron/augustin-de-romanet>
  - Jean-françois Cirelli (directeur adjoint de cabinet du Premier ministre Jean-Pierre Raffarin, PdG Gaz de france->suez, actuellement PDG de BlackRock france. (?? Influent du dossier "Retraite"; **membre du Comité action Publique 2022 en 2018** <https://fr.wikipedia.org/wiki/Comit%C3%A9\_action\_publique\_2022> ???))
  - Thierry Breton (France telecom puis ministre de l'économie.. Et maintenant Président du Directoire d’Atos Origin.
  Extrait du site d'Atos, Jean-Philippe Thierry, Président du Conseil de Surveillance, a déclaré : 
      "*M. Thierry Breton apporte une précieuse expérience d'entrepreneur et d'industriel qu'il a constituée à la tête de grandes entreprises du secteur de l'informatique, des nouvelles technologies et des télécoms, ainsi qu'à l'international. Il pourra s'appuyer sur les bases solides existantes afin de donner une nouvelle dimension au Groupe. Il saura donner l'impulsion nécessaire à l'entreprise pour faire face aux défis de l'actuel environnement économique et engager une nouvelle étape de son développement.**underline L'ensemble des membres du Conseil de Surveillance et moi-même avons toute confiance en M. Thierry Breton pour rapidement maximiser la création de valeur pour tous les actionnairesunderline***". (**DANS TOP 10 DES FDP** !!)
  <https://atos.net/fr/2008/communiques-de-presse/informations-financieres-communiques-presse/relations-investisseurs\_2008\_11\_16/pr-2008\_11\_16\_01> 
  - MATTHIEU PIGASSE (privatisation du Crédit lyonnais, de France Télécom, de Thomson-CSF, création d’Areva, fusion Vivendi-Universal...)
- Gilles Le-Gendre devait être président de la commission de surveillance de la CDC mais conflit d'intérêt trop évident donc EXIT. 
  MAIS il est remplacé par Sophie Errante (LReM) moins connu médiatiquement ( *« Il fallait quelqu'un qui ait envie de cet engagement et qui soit, par nature, davantage dans l'ombre que dans la lumière médiatique », *justifie-t-elle. source <https://www.lesechos.fr/2018/04/sophie-errante-une-elue-de-terrain-a-la-cdc-988584> ) 
  En fouillant un peu : son mari est président PHI <https://dirigeant.societe.com/dirigeant/Edouard.ERRANTE.86402532.html>
  Et elle (avec son mari) de SOCIETE CIVILE IMMOBILIERE ELIVAL <https://www.societe.com/societe/societe-civile-immobiliere-elival-500637269.html>                                          
- Rappel de la fonction de la cours des comptes. <https://www.ccomptes.fr/fr/cour-des-comptes/role-et-activites>
- Quelles sont les liens entre la cours des comptes et le "Haut Conseil des Finances Publiques"? <https://www.hcfp.fr/>
   
- une enquête en 3 volets mené par Basta mag ! Sur la monopolisation des enarque à l'inspection des finances et le pantouflage chronique dans les banques 

  * <https://www.bastamag.net/Ces-enarques-charges-de-piloter-la-politique-economique-de-la-France-qui>
  * <https://www.bastamag.net/Inspecteurs-des-finances-une-caste-d-elite-qui-a-pour-mission-principale-de>
  * <https://www.bastamag.net/Le-pantouflage-a-l-Inspection-des-finances>

## Actions possibles :

Propositions :

- Ouvrir les actions devant le tribunal correctionnel pour délit de prise illégale d'intérets à des associations, citoyens
- Délai de carence entre prise de fonction dans le secteur privé et l'IGF quand l'activité concerne dans le privé justement des dossiers traités à Bercy
- Contacter la sénatrice Marie-France Beaufils qui a fait un rapport sur le CICE
<https://www.humanite.fr/marie-france-beaufils-les-fonds-publics-ne-peuvent-etre-verses-sans-verification-de-leur-utilisation>
- Faire des interviews de personnes calé sur le sujet pour un éventuel reportage ?
- Inciter la presse/youtube/autres à parler du sujet
- Promouvoir la défection en mettant à jour les lâchages : donner des garanties aux lanceurs d'alerte
- Faire une vidéo PuTaClik genre (le Top 10 des plus gros FDP en pantoufle/costard (qui s'en mettent pleins les fouilles) !)

Biblio

- Laurent Mauduit - La Caste -  Enquête sur cette haute fonction publique qui a pris le pouvoir.
- Marc Endeweld 
- <https://www.alternatives-economiques.fr/hauts-fonctionnaires-preferent-prive/00079448> ++++
- <http://tnova.fr/rapports/les-conflits-d-interets-nouvelle-frontiere-de-la-democratie>
- <https://youtu.be/e0L8CmSnSuQ> ARTE - l'urgence de ralentir (marchés financiers qui mettent en danger les échanges monétaires par la spéculation)
- <https://www.lepetitjuriste.fr/fraude-fiscale-verrou-de-bercy-a-t-vraiment-saute/> analyse sur la loi qui desserre le verrou de Bercy
