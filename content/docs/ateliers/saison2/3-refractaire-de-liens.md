---
title: 3 - Les Réfrac'terre de liens
---

## Problèmes

- Gros problème de renouvellement au niveau des paysans/pas de repreneur/les structure grossissent et deviennent inreprenables par des particuliers. "Du coup c'est des gros groupes qui les reprennent". (Monopolisation de la production)
- PAC, politique agricole commune. Allemagne et France touche plus que l'Angleterre à l'époque.. 10 milliard pour la France. (Une partie est donné par l'europe et une partie par la région) RISQUE DE DERIVE, Y a t il un suivi?oui 
- Plus de subventions donné pour les productions conventionnelles  tel que le maîs les céréales les plus productives  cercle vicieux (plus on fait de cultures plus on a d’animaux plus on touche de subventions)

## Questions

- Comment fait-on pour sortir de ce cercle vicieux où on a  de moins en moins de paysans et des structures sont de plus en plus grosses, comment montrer l'attractivité du travail agricole ?
- Comment fait on comprendre que c'est l’assujettissement de l'agriculture au marché néolibéral faussé par le dumping  qui entraîne surproduction, pollution, mal être des travailleurs, mauvaise santé, disparition des cultures vivrières spécialisation famines (pandémies?) etc...
- comment effectuer un "retour à la terre" dans les meilleures conditions possibles?

## Actions existantes

- Il existe la Coopérative d'initiative agriculture paysanne. c'est un outil qui permet de prendre plus de temps pour se former et se projeter sur la ferme à reprendre, le futur repreneur d'abord stagiaire de sa futur ferme gagne un an d’expérience sans la pression économique, contrat de parrainage durée 1 à 2 ans idem
- Idée au projet: proposé par les réseaux d'agriculture paysanne pour débroussailler le parcours à l'installation
PAT Politique Agriculture du Territoire les agglos peuvent être les premiers acteurs
Veille foncière (départ retraite, friche, foncier) maires conseillers municipaux
Ceinture Alimentaire (ou Maraîchère) [ <http://www.ceinturealimentaire.be/> || <https://www.catl.be/> || <https://www.colibris-lemouvement.org/passer-a-laction/creer-son-projet/developper-une-ceinture-maraichere-autour-villes> || ... ]    
<https://terresenvie.fr/>
<https://parcel-app.org/index.php>  outil pour évaluer les besoins et le potentiel d'un territoire pour avancer dans l'autonomie alimentaire
- Novembre 2021 mois de l'installation accueil gd public dans des fermes et cafés installation le soir (Bretagne collectif paysan : cedapa, conf, agripaysanne, ciap, accueil paysan terre de liens)

## Action à venir ou lancement d'initiative nouvelle
Mettre à mal les structures agricole et para-agricole (En particulier 4 groupes dit les 'BIG FOUR" ADM, Bunge, Cargill et Dreyfus+ Vincent Bolloré via Socfin et ses filiales Socfinaf et Socfinasia) qui n'ont aucun intérêt à ce que les moyens de productions évolue vers des productions durable : 

- Commencer par informer/dénoncer pour amener au boycott ! 
- Comprendre qu'elles sont les terres qu'ils ont, en France dans un premier temps, et les reprendre ! (Récupérer nos terres pour pouvoir y mener une politique de production responsable vis à vis de la terre/vie.

**Vulgarisation :** comment fonctionne l’accès au foncier : Revue dessinée : parcours d'installation d'un jeune agriculteur

**Vulgarisation :** le fonctionnement de notre agriculture : Comment ça marche en France <https://fr.wikipedia.org/wiki/Agriculture\_en\_France> <https://www.confederationpaysanne.fr/rp\_article.php?id=10207>

**Sanctuarisons nos terres agricoles :** achat des grande surface de foncier agricole par les grands industriel, par la couronne d'Angleterre et par la chine. <https://www.bastamag.net/Bollore-Credit-agricole-Louis>

**Sanctuarisation de la ressource en eau :** reconnaissance de bien commun (cf italie) <https://www.bastamag.net/Eau-comment-les-Italiens-ont-dit>

**Les Communs :** <https://reporterre.net/Face-au-capitalisme-se-federer-pour-le-salut-commun>, conférence Benjamin Coriat
<https://www.youtube.com/watch?v=JRkOWykAuZc>

<https://impactons.debatpublic.fr/>, il y a une proposition gouvernementale de participer à la réflexion concernant la future politique agricole commune, ça à l'air bidon mais si on l'utilise massivement de façon concerté ???

## Parti-Prenantes (Agricole ou ONG)

- **Terre de liens**: outil qui permet de soulager l'investissement du paysan lié au foncier, bail environnemental cela bloque les surface en AB. Au niveau national. les structures sont propriété de la foncière terre de liens , en Bretagne le foncier appartient à des GFA (groupement foncier agricole) et SCI (société civile immobilière) qui sont composés de sociétaires principalement proches du fermier qui s'engagent pour 3 ou 5 ans dans le capital foncier du paysan. les structures citoyennes sont composés de  50 à 400 sociétaires <https://terredeliens.org/bretagne.html> 

- **RENETA** (Réseau National des Espaces Tests Agricoles) : 06 78 53 45 58 contact@reneta.fr  www.reneta.fr

- **Super Local**: vidéo d'introduction de partagez c'est sympa <https://www.youtube.com/watch?v=bgJZ1yHAxB0>, <https://superlocal.team/>

- **Les parc inter régionaux :**

- **SAFER :** Un outil français permettant de mientenir le prix du foncier agricole, encadrement des PLU, mais c'est aussi une faille permettant au gros propriétaire terrien (industriel, FNSEA) d'agrandir leurs exploitation

- **MSA :** Sécurité social pour les Agris qui étouffe les petits au profit des gros, voir le Projet de Bernard friot

- **PAC (Europe) :** impactons nous réflexion sur la future PAC 

- **FEOGA**(PAC): Fond Européen Organisation et de Garentie Agricole

- **IMPACT, RAD**: réseau de l'agriculture durable

- **Semances Paysane**: 

- **Kokopelli**: 

- **Confédération Paysanne**: les amis de la conf, scandal :, énorme inertie des changements politiques milieu agricole même progressif.

- **biolait**: coopérative laitière bio pouvoir des producteurs mais politique de production qui doit suivre les concurrents 

- **Ver de terre production :** analyse critique et veille techno voir recherche en agriculture

- **L'atelier Paysan :** l'art du DIY pour la fabrication d'outils agricoles

- **Brin de Paille + Université Populaire de Permaculture :** ils sont complètement dans les choux la structure est complètement sclérosé

- **FAO :** <http://www.fao.org/3/I9542FR/i9542fr.pdf>

- **afog :** centre de gestion alternatif pour l'autonomie en compta gestion des agris

## Scandales
<https://www.slate.fr/story/186413/pac-politique-agricole-commune-millions-euros-detournement-fonds-corruption-scandale-europeen>

- **Ferme d'avenir :** piloté par Maxime de Rostelan une Permaculteur "en marche", organisation de l'an zero, la Bascule 

- **Pierre Rabhi :**  

- **Cyril Dion :**  

- **ESS :** Economie Social et Solidaire, financeur (green Wacher)

- **Les Bassines :** voir BNM (invivo) <https://bassinesnonmerci.fr/>

- **Lactalis :** Scandale sur scandale

- **Biocop :** Répond malgré tout à un marché (exemple agriculteur, les paysans qui avaient monté une biocoop coopératives dont les statuts interdisait l'emballage plastique mais une fois reprise par le réseau national cette volonté n'était plus respectée. très hétérogène d'une biocoop à l'autre 

- **Lessieur :** Huile minérale, dans les Huiles alimentaires <https://www.agoravox.fr/tribune-libre/article/le-scandale-du-siecle-43204>

- **Les BIG FOUR :** <https://www.franceculture.fr/emissions/la-bulle-economique/les-big-four-et-le-marche-des-cereales>

- **Embalage des produit Bio :** pour la grande surface les producteurs bio doivent emballer 

- **Embalage frauduleux**: les faux label

- **2012 :** comment le bio s'est ouvert à l'agro industrie, cahier des charges harmonisé

- **Qui paye les certification ?** les agriculteurs BIO et non une taxe sur les conventionnel

- **Glyphosate :** Etude de Gilles Eric Seralini : 2018 <https://www.marianne.net/societe/le-mais-ogm-cancerigene-scientifiquement-dementi> 2012 <https://www.marianne.net/societe/ogm-l-allemagne-recale-l-etude-seralini> les pisseurs volontaires (Monsieur SAM) 

- **Semmence :** Catalogue oficielle, Pascale Poot, Kokopelli process

- **Achat des terres puissance étrangère ou des industriels :** <https://www.youtube.com/watch?time\_continue=27\&v=acYN-vn9RzU\&feature=emb\_title>

- **Triskalia :** <https://reporterre.net/Le-drame-des-salaries-de-Triskalia-intoxiques-aux-pesticides>: cette affaire extrêmemment dégueu est révélatrice de l'ambiance ds l'agroindus

- **COVID19 :** la mauvaise gestion prévisible des productions alimentaire pendant le confinement  : <https://www.pleinchamp.com/grandes-cultures/actualites/que-faire-des-450-000-tonnes-d-excedent-de-pommes-de-terre-francaises>
   
## Bibliographie
- <https://resiliencealimentaire.org/covid-19-qui-veille-au-grain-pour-demain-les-propositions/>
- <https://fr.wikipedia.org/wiki/Agriculture\_en\_France>
- <https://ec.europa.eu/eip/agriculture/sites/agri-eip/files/synth\_le\_devenir\_des\_agri\_hcf.pdf>
- <https://www.persee.fr/doc/ecoru\_0013-0559\_2000\_num\_255\_1\_5151>

## Agenda 
- Congrès de la FNSEA : à Niort <https://www.lanouvellerepublique.fr/niort/le-congres-national-de-la-fnsea-aura-lieu-a-niort-en-2020>
- futur réforme de la politique agricole commune 2021 ? <https://impactons.debatpublic.fr/>
    
## Les Chiffres pour mieux comprendre : 
## la SAU :
- <https://fr.wikipedia.org/wiki/Surface\_agricole\_utilis%C3%A9e#SAU\_au\_niveau\_du\_globe>
- La SAU française5 représente environ 29 millions d'hectares, soit environ la moitié (54 %) du territoire national. Elle se répartit en terres arables pour 62 %, en surfaces toujours en herbe pour 34 % et en cultures pérennes pour 4 %. Cette surface totale se décompose en :
- Terres arables (18,4 millions d'ha) dont céréales (9,4), prairies temporaires et fourrages annuels (4,9), oléagineux (2,3), autres cultures annuelles (1,3) et jachères (0,5) ;
- Cultures permanentes : vergers, vignes (1) ;
- Prairies permanentes (7,7) ;
- Autres surfaces agricoles utilisées hors exploitation (1,9) dont prairies collectives et hors champ (1,8) jardins et vergers familiaux (0,1).
- Carte : <https://www.geoportail.gouv.fr/carte?c=-1.502475555322755,46.20938367513466\&z=14\&l0=GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2::GEOPORTAIL:OGC:WMTS(0.54)\&l1=LANDUSE.AGRICULTURE2018::GEOPORTAIL:OGC:WMTS(0.73)\&permalink=yes>
