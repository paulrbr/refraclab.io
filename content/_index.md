---
title: Le discord du Canard Réfractaire
type: docs
---

# Bienvenue à vous, canar(a)des réfractaires !
>
> Ce site web est l'espace de publication des Réfractaires, qui est un groupe constitué sur le discord du Canard Refractaire, un media indépendant des Côtes d'Armor.
>

#### Mais quel est donc l'objectif de ce discord?

Ici, chaque membre de la communauté peut partager des articles, des vidéos, ou autres, qui peuvent être débattus par la suite.
Par exemple, il peut nous arriver de faire un travail de journaliste, tous ensemble, en croisant nos sources ou de réfléchir ensemble sur un sujet particulier.

#### Pour autant, est-ce l'objectif principal ?

Difficile de vous répondre, car il n'y pas d'objectif réellement prédéfinis ; c'est en quelque sorte la communauté qui définit ses propres objectifs et tente de trouver SA voie. L'écosystème se met en place par lui-même, et cela nous paraît essentiel.

#### Peut-on parler d'intelligence collective ?

Et bien, laissez-vous voguer et tenter d'apporter votre pierre à l'édifice, par n'importe quels moyens.
Avant toute chose, sachez que personne n'est maître de ce serveur - pas de verticalité, pas de hiérarchie, pouvoirs très limités. Cependant, il y a des règles de respect mutuel.

# Règlement en vigueur sur le discord du Canard Réfractaire

- Le respect est quelque chose de très important. La courtoisie et le savoir-vivre sont de rigueur
- La diffamation et la propagande excessives sont également proscrites sur le serveur.
- Il est strictement interdit de divulguer sans autorisation des informations personnelles ou privées d'une autre personne, tout comme diffuser dans les canaux publics des discussions ou éléments de discussions privées.
- Evitons les promotions parasites, à l'exception des liens Discord ou vers d’autres médias indépendants et engagés.
- Les propos non sourcés n'ont aucune intérêt pour notre travail collectif et seront considéré au même tire que les messages parasites
- L'abus de l'écriture en majuscules, de retour à la ligne et d'emojis est déconseillé.
- Pensez à supprimer ou modifier votre message si vous vous trompez.

# Canaux disponibles

## Accueil

- `accueil` vous y êtes invité à lire et cocher donc accepter le règlement qui s'y trouve
- `vidéos-du-canard`  un bot nous averti lorsqu'une vidéo est publiée sur la chaîne YouTube
- `annonce` les événements du discord
- `médias-des-copains` les liens vers les partenaires et les média indépendants amis,
- `revue-des-réfractaires` est un filtrage des meilleurs articles, vidéos, etc postés dans les canaux thématiques et vérifiés par la communauté pour vous proposer des informations qui en feront cancaner plus d'un !
- `répertoire-médiatique` est un annuaire collaboratif

## Agora

C'est un espace de discussion avec un maximum d'auto-gestion/de libertés :

- `blabla` on y discute de tout et de rien
- `discussions-générales` on y discute de rien et de tout
- `idees` ici les idées : révolutionnaires, transitoires, alternatives ou bisounours
- `vos-medias` on y partage des articles interessants (peut servir a alimenter la revue de presse)
- `vecu` on y partage des experiences personnelles, des anecdotes...
- `fact-checking` on fact-check des documents
- `humour` .... faut il préciser?
- `musique` on y partage nos musique, éventuellement on en discute
- `retours-sur-l'emission` on discute ici des dernieres videos du Canard Refractaire
- `compte-rendu-de-discussion` on partage ici les comptes rendus des dernieres reunions

et quelques canaux vocaux

- `vocal-accueil` lie au canal `accueil-ecrit`, des refractaires pourront cous accueillir ici.
- `vocal-1-[détendu]` lié au canal `écrit-vocal-1`. Sur le vocal on discute où on vient juste écouter et sur l'écrit on participe par écrit, on poste les liens. 
- `vocal-2-[atelier]` lié au canal `écrit-vocal-2`. Un espace pour s'exprimer à condition d'accepter le cadre choisi par le groupe (travail, brain-storming, tour de parole, réunion...). 
Quand on est nombreux on passe par l'écrit pour demander la parole qui est attribuée par le facilitateur.
- `vocal-3-[sans écrit]` l'option favorite de qui veut rester sur smartphone, sans se soucier des règles de prise de parole, ni des interventions écrites.
- `Débat` sert lorsqu'un débat est organisé ou s'improvise lors d'une rencontre électrique sur une thématique ou sur un canal de la revue de presse.
- `Enregistrement` est destiné aux enregistrement (de podcast), si vous arrivez sachez que ça enregistre (ou pas) c'est autogéré.

## Revue-de-presse

Cette section se structure en canaux de débats thématiques uniquement accessible aux membres ayants le role `LES REFRACTAIRES`, nous vous demandons de bien vouloir respecter ces thématiques autant que possible afin de faciliter la modération et de ne pas rendre les canaux illisibles.

- On poste les média au bon endroit.
- On vérifie que le lien n'a pas déjà été posté.
- On peut discuter mais on évite de "flooder" le canal ou de poster un même contenu dans différents canaux. S'il y a trop d'échanges on se recentre sur un canal de l'agora en invitant les gens à venir échanger.

## Educ-pop

cette section sert a faire de l'education populaire, pratique d'education horizontale remise au gout du jour par Franck Lepage ([video](https://youtu.be/q2VyR875gwU)), ici on s'eduque entre nous!

## Redaction collaborative

cette section sert a aider yohan a rediger ces videos a l'aides de salon temporaire et thematiques qui serviront a collecter des informations sur un sujet donne.

# Rôles

Entrer dans la communauté vous laisse la possibilité de jouer un de ses rôles à nos côtés ou juste venir vous informer. Aucune obligation n'est imposée.

- **Les Membres** : en vous inscrivant si vous signez la charte vous devenez membre, ce choix vous permet d’accéder aux canaux précédents et d'y participer.
- **Les Réfractaires** : sont des membres qui ont été identifiés par la communauté pour leur engagement et leur participation active. Cette reconnaissance permet de contribuer à d'autres canaux et d'autres rôles :
  - **l'Accueil** : ce rôle est d'accueillir les nouveaux arrivants. Ils ont la possibilité d'attribuer le rôle Réfractaire.
  - **La Curation** : tient à jour `revue-des-réfractaires` entretenu et agréable à la lecture.
  - **La Facilitation** : fourni un cadre coopératif, accompagne les groupes et leur apporte les outils et la démarche adaptées aux missions qui leur sont confiées.
  - **Le Fact-checking** : vérifie les faits et sensibilise les membres publiant des articles non sourcés, et redirige au besoin vers la catégorie `documents-controversés`
  - **Les Techos** : conçoivent et améliorent les outils numériques à disposition du groupe pour pouvoir coopérer et fonctionner ensemble efficacement.
- **Les Modérateurs** : ont pour rôle de garantir le bien être de tous. Ils veillent au respect de la charte du discord. Suppriment les messages inutiles dans les canaux revue de presse (ces derniers sont alors sauvegardé et un lien vers la sauvegarde est déposé à la place). Le cas échéant ils peuvent sanctionner les comportements toxiques / sources de tensions mais uniquement en dernier ressort. Ils peuvent vous envoyer au   où vous ne verrez plus qu'un seul canal. Dans le marais-cage, vous pourrez discuter avec les réfractaires. Ceux-ci assureront la médiation avant de décider de vous réintégrer ou de vous bannir.
- **Les Plombiers** : ont les memes droits que les moderateurs mais sont la pour s'occuper de nos petits bots d'amour &#x2764;
- **Les Concierges** : assurent le bon fonctionnement du serveur.

# Bots

Vous pourrez apercevoir sur ce serveur discord quelques membres un peu bizarre avec une petite etiquette `BOT`, ce sont des robots c'est a dire des programmes informatiques qui repondent a des commandes pour aider a gerer le discord (ou pas). Certains de ces bots sont crees par nos soins (donc open-sources evidemment) et auto-heberges, d'autres sont des bots externes de grandes ampleurs presents sur des millions de serveurs.

- **collinks** ce bot (cree par @12b &#x2764; et @mose &#x2764;) est la pour collecter tout les liens postes dans le discord et les ranger dans une base de donnee pour pouvoir en faire la curation plus tard. Ce bot est auto-heberge et open-source. [sources](https://gitlab.com/crapaud-fou/collinks)
- **mechanical duck** ce bot (cree par @00101010) a un but de moderation, il permet d'attribuer temporairement un role a un membre du serveur. Il est auto-heberge et open-source. [sources](https://gitlab.com/refrac/mechanical-duck)
- **MEE6** ce bot est un bot externe de moderation. nous n'avons pas acces aux sources et il est heberge par l'entreprise MEE6. [site](https://mee6.xyz/)
- **Rythm** ce bot est un bot externe qui sert a jouer des videos(juste le son) a partir de youtube dans un vocal, il est uniquement utilisable dans les canaux refractaires. [site](https://rythmbot.co/)
- **Sandrine Caneton** ce bot (code par [moonstar-x](https://github.com/moonstar-x) et forke par @00101010 pour l'adapter a nos besoins) est un bot de text-to-speech c'est a dire qu'il sert a lire du texte avec une voix synthetique dans un vocal, il est utilisable par tout les membres a l'aide de la commande $say. ce bot est auto-heberge et open-source [sources](https://github.com/00I0I0I0/sandrine-caneton)\
&#x26A0; ce bot utilise l'API de google translate pour generer la voix.
- **YAGPDB.xyz** ce bot est un bot externe de moderation. nous n'avons pas acces aux sources et il est heberge par l'entreprise botlabs. [site](https://yagpdb.xyz/)